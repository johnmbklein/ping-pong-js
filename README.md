# Ping Pong Game

#### Substitues certain numbers generated from user input for "ping" "pong" and "pingpong."

#### By John Klein

## Description

This app generates a list of numbers from 1 to the user specified maximum number. Multiples of 3, 5, and 15 are replaced with "ping," "pong," and "pingpong," respectively, and then output to the page. It isn't really a "game."

## Setup/Installation Requirements

This web app requires Javascript. No installation necessary--just go to http://johnmbklein.github.io/ping-pong/ to check it out!

## Known Bugs

There are no known bugs at this time.

## Support and contact details

Contact me if you find a bug or have any feedback.

## Technologies Used

This app was built using, HTML, CSS, bootstrap, jQuery, and Javascript.

### License

This app is licensed under the GPL. See the license file for more information.

Copyright (c) 2016 John Klein
